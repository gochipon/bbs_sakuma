<?php

/**
 * Thread actions.
 *
 * @package    BBS
 * @subpackage Thread
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ThreadActions extends sfActions{
  
  public function executeNew(sfWebRequest $request)
  {
    $form_mng = new FormManage(FormManage::THREAD);
    $db_mng   = new DataBaseManage();
    
    if($form_mng->isInputCheck($request) == true){
//      $db_mng->InsertThread($form_mng->getUseForm()->getValues());
      $db_mng->Insert($form_mng->getUseForm()->getValues());
      $this->redirect('Creat');
    }
    
    // バリデーションされたフォームの値をテンプレートに渡す
    $this->form = $form_mng->getUseForm();
    $this->db_mng = $db_mng;
  }
  
}


