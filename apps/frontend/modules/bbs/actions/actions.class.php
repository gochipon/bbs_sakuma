<?php

/**
 * bbs actions.
 *
 * @package    BBS
 * @subpackage bbs
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class bbsActions extends sfActions
{
  
  public function executeThread(sfWebRequest $request){
    $form_mng = new FormManage(FormManage::THREAD);
    $db_mng   = new DataBaseManage();
    
    if($form_mng->isInputCheck($request) == true){
      $db_mng->InsertThread($form_mng->getUseForm()->getValues());
      $this->redirect('Creat');
    }
    
    // バリデーションされたフォームの値をテンプレートに渡す
    $this->form = $form_mng->getUseForm();
    $this->db_mng = $db_mng;
  }
  
  public function executeResponse(){
    
  }
    
}
