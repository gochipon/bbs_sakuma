<?php

class ThreadResponseActions extends sfActions
{
  public function executeScreen(sfWebRequest $request)
  {
    $form_mng = new FormManage(FormManage::THREAD_RESPONSE);
    $db_mng = new DataBaseManage();
    $thread_title = $request->getParameter('value');
    
    $threadRecord = ThreadTable::getInstance()->findOneById($thread_title);
    
    if($form_mng->isInputCheck($request) == true){
      $db_mng->InsertRes($threadRecord->getId(), $form_mng->getUseForm()->getValues());
      $this->redirect('Creat');
    }
    
    $this->form = $form_mng->getUseForm();
    $this->db_mng = $db_mng;
    $this->thread_title = ThreadTable::getInstance()->findOneById($thread_title);

  }
}
