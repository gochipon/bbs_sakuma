<?php

class ThreadForm extends BaseForm{
  
  public function configure(){
      // ただのフォームの枠
//    $this->setWidgets(array(
//        'name'  =>  new sfWidgetFormInput(),
//        'title' =>  new sfWidgetFormInput(),
//        'body'  =>  new sfWidgetFormTextarea(),
//        ));
//    $this->setValidators(array(
//        'name'  =>  new sfValidatorString(),
//        'title' =>  new sfValidatorString(),
//        'body'  =>  new sfValidatorString(),
//        ));
    
    // プロパティ設定付きのフォームの枠
    $this->setWidgets(array(
        'name'  =>  new sfWidgetFormInput(array(), array(
                          'size'      => 20,
                          'maxlength' => 10, 
                        )),
        'title' =>  new sfWidgetFormInput(array(), array(
                          'size'      => 50,
                          'maxlength' => 100,
                        )),
        'body'  =>  new sfWidgetFormTextarea(array(), array(
                          'cols'      => 100, // 横
                          'rows'      => 20,  // 縦
                        )),
        ));
    $this->setValidators(array(
        'name'  =>  new sfValidatorString(array(
                          'required'  => true,
                          'max_length'=> 20,
                          ),array(
                          'required'  => '未入力',
                          'max_length'=> '20文字以内で入力してください',
                          )
                        ),
        'title' =>  new sfValidatorString(array(
                          'required'  => true,
                          'max_length'=> 100,
                          ),array(
                          'required'  => '未入力',
                          'max_length'=> '100文字以内で入力してください',
                          )
                        ),
        'body'  =>  new sfValidatorString(array(
                          'required'  => true,
                          'max_length'=> 1000,
                          ),array(
                          'required'  => '未入力',
                          'max_length'=> '1000文字以内で入力してください',
                          )
                        ),
        ));
    
    $this->widgetSchema->setNameFormat('thread[%s]');
    // フォームにラベルを設定
    $this->widgetSchema->setLabels(array(
        'name'  =>  'お名前',
        'title' =>  'タイトル',
        'body'  =>  '書き込み',
        ));
  } 
}
