<?php

/*
 * 
 */
class FormManage
{
  const THREAD = 'Thread';
  const THREAD_RESPONSE = 'ThreadResponse';
  private $use_form;
  
  
  public function __construct($select_name){
    $class_name = $select_name .'Form';
    $this->use_form = new $class_name();
  }
  
  public function getUseForm(){
    return $this->use_form;
  }
  
  /**
   * 詳細：入力された値を検査する<br>
   * [処理メモ]<br>
   * 1,リクエストメソッドがPOSTか。<br>
   * 2,getParameterでsetNameFormatメソッドで設定した配列の値を全て取得し
   *   フォームインスタンスへバインドする<br>
   * 3,バインドされた値に問題はないか検証(フォームに値がバインドされた時のみ有効)
   * @param object $request リクエストクラス
   */
  public function isInputCheck(sfWebRequest $request){
    if($request->isMethod(sfRequest::POST)){
      $this->use_form->bind($request->getParameter($this->use_form->getName()));
      if($this->use_form->isValid()){
        return true;
      }
    }
  }
  
}