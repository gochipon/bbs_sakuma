<?php

class ThreadResponseForm extends BaseForm{
  public function configure(){
    
    $this->setWidgets(array(
        'name'  =>  new sfWidgetFormInput(array(), array(
                          'size'      => 20,
                          'maxlength' => 10, 
                        )),
        'body'  =>  new sfWidgetFormTextarea(array(), array(
                          'cols'      => 100, // 横
                          'rows'      => 20,  // 縦
                        )),
        ));
    $this->setValidators(array(
        'name'  =>  new sfValidatorString(array(
                          'required'  => true,
                          'max_length'=> 20,
                          ),array(
                          'required'  => '未入力',
                          'max_length'=> '20文字以内で入力してください',
                          )
                        ),
        'body'  =>  new sfValidatorString(array(
                          'required'  => true,
                          'max_length'=> 1000,
                          ),array(
                          'required'  => '未入力',
                          'max_length'=> '1000文字以内で入力してください',
                          )
                        ),
        ));
    
    $this->widgetSchema->setNameFormat('thread_response[%s]');
    // フォームにラベルを設定
    $this->widgetSchema->setLabels(array(
        'name'  =>  'お名前',
        'body'  =>  '書き込み',
        ));
  } 
}
