<?php

class DataBaseManage{
  // 選択肢(これを使ってに処理を分岐させる)
  const THREAD = 'Thread';
  const RESPONSE = 'ThreadResponse';
  private $db_table_box;
  
  /**
   * 
   * @param type $db_select
   * @return type
   */
//  public function __construct(){
//    $table_name = array('Thread' => 'Title', 
//                        'ThreadResponse' => 'Id',
//                  );
//    foreach($table_name as $list => $key){
//      $table = $list .'Table';
//      $find  = 'findOneBy' .$key;
//      $this->db_table_box = array(
//                              array($table::getInstance(),
//                                    $table::getInstance()->$find())
//                            );
//    }
//  }
  
  public function __construct(){
    $table_name = array('Thread' => 'Title',
                        'ThreadResponse' => 'Id',
                  );
    foreach($table_name as $list => $key){
      $table = $list .'Table';
      $this->db_table_box[] = array($table::getInstance(), $key);
    }
  }
  
//  public function __construct(){
//    $table_name = array('Thread',
//                        'ThreadResponse',
//                  );
//    foreach($table_name as $list){
//      $table = $list .'Table';
//      $this->db_table_box[] = $table::getInstance();
//    }
//  }
 
  /**
   * インサートするっす
   * 
   * @param type $values
   */
  public function InsertThread($values){
    $table = ThreadTable::getInstance();
    $doc_obj = $table->getConnection();
    $doc_obj->insert($table, array('title' => $values['title'],
                                   'user'  => $values['name'],
                                   'body'  => $values['body'],
                             )
                    );
  }
  
  public function InsertRes($id, $values){
    $table = ThreadResponseTable::getInstance();
    $doc_obj = $table->getConnection();
    $doc_obj->insert($table, array('title' => $id,
                                   'user'  => $values['name'],
                                   'body'  => $values['body'],
                             )
                    );
  }
  
  
  public function Insert($values){
  
//    for($index=0; $index>2; $index++){
//      $doc_obj = $this->db_table_box[$index]->getConnection();
//      $doc_obj->insert($this->db_table_box[$index], array('title' => $values['title'],
//                                                          'user'  => $values['name'],
//                                                          'body'  => $values['body'],
//                                                    )
//                      );
//    }
    
    
    
    for($index=0; $index<2; $index++){
      // どうやら書き込む前のためIdを取得できないっぽい
      exit(var_dump($values));
      $box = ThreadTable::getInstance()->findOneById($values['title']);
//      $box = $this->db_table_box[$index][0]->findOneById($values['title']);
      exit($box);
      $doc_obj = $this->db_table_box[$index][0]->getConnection();
      $doc_obj->insert($this->db_table_box[$index][0], array('title' => $box,
                                                          'user'  => $values['name'],
                                                          'body'  => $values['body'],
                                                    )
                      );
    }
  }
  
  
  public function FindAll(){
    return ThreadTable::getInstance()->findAll();
  }
  
  public function FindResAll(){
    return ThreadResponseTable::getInstance()->findAll();
  }
  
  public function Find($select){
    switch($select){
      case self::THREAD:
        return ThreadTable::getInstance()->findAll();
      case self::RESPONSE:
        return ThreadResponseTable::getInstance()->findAll();
    }
  }
  
  
  public function Draw($thread_title){
   foreach($this->Find(self::RESPONSE) as $res_copy):
      if($res_copy->getTitle() == $thread_title->getId()){
        echo $res_copy->getUser()."<br>";
        echo $res_copy->getBody()."<br><br>";
      }
    endforeach;
  }
  
}